package com.qualitype.projectSample;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.qualitype.projectSamples.Evaluation;
import com.qualitype.projectSamples.Result;
import org.junit.jupiter.api.Test;

public class EvaluationTest {

  @Test
  public void evaTest1() {
    var eva = new Evaluation();
    assertEquals(Result.UNGÜLTIG, eva.evaluationMeasurement(6, 39, 9, 10));
  }

  @Test
  public void evaTest2() {
    var eva = new Evaluation();
    assertEquals(Result.UNSICHER, eva.evaluationMeasurement(6, 39, 9, 22));
  }

  @Test
  public void evaTest3() {
    var eva = new Evaluation();
    assertEquals(Result.UNSICHER, eva.evaluationMeasurement(6, 4, 29, 39));
  }

  @Test
  public void evaTest4() {
    var eva = new Evaluation();
    assertEquals(Result.UNSICHER, eva.evaluationMeasurement(28, 17, 9, 20));
  }

  @Test
  public void evaTest5() {
    var eva = new Evaluation();
    assertEquals(Result.POSITIV, eva.evaluationMeasurement(6, 39, 34, 30));
  }

  @Test
  public void evaTest6() {
    var eva = new Evaluation();
    assertEquals(Result.POSITIV, eva.evaluationMeasurement(40, 39, 9, 33));
  }

  @Test
  public void evaTest7() {
    var eva = new Evaluation();
    assertEquals(Result.POSITIV, eva.evaluationMeasurement(23, 6, 34, 27));
  }

  @Test
  public void evaTest8() {
    var eva = new Evaluation();
    assertEquals(Result.POSITIV, eva.evaluationMeasurement(20, 39, 20, 22));
  }

  @Test
  public void evaTest9() {
    var eva = new Evaluation();
    assertEquals(Result.NEGATIV, eva.evaluationMeasurement(6, 2, 9, 31));
  }
}
