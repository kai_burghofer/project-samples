package com.qualitype.projectSamples;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@RequestScoped
@Transactional
@Path("/samples")
public class SamplesResource {
  @PersistenceContext
  private EntityManager em;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getAllSamples() {
    try {
      final var query = createSelectQuery();
      final var result = query.getResultList();
      final var samples = result.stream().map(this::allToSample).collect(Collectors.toList());
      return Response.ok(samples).build();
    } catch (final Exception ex) {
      return Response.status(Status.BAD_REQUEST).build();
    }
  }

  private TypedQuery<SampleEntity> createSelectQuery() {
    final var jpaql = "SELECT s FROM Sample s ORDER BY s.id";
    final var query = this.em.createQuery(jpaql, SampleEntity.class);
    return query;
  }

  @GET
  @Path("{sampleId}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getOneSample(@PathParam("sampleId") final String sampleId) {
    final var entity = findBySampleId(sampleId);
    if (entity != null) {
      final var value = allToSample(entity);
      value.descriptions = entity.getDescriptions();
      return Response.ok().entity(value).build();
    }
    return Response.status(Status.NOT_FOUND).build();
  }

  @GET
  @Path("/statistic/all")
  public Response countSamples() {
    try {
      final var jpaql = "SELECT COUNT(s) FROM Sample s";
      final var query = this.em.createQuery(jpaql, Long.class);
      final var result = query.getSingleResult();
      return Response.ok(result).build();
    } catch (final Exception ex) {
      return Response.status(Status.BAD_REQUEST).build();
    }
  }

  @GET
  @Path("/statistic/open")
  public Response countOpenSamples() {
    try {
      final var jpaql = "SELECT COUNT(s) FROM Sample s LEFT JOIN s.measurementEntity m WHERE m IS NULL";
      final var query = this.em.createQuery(jpaql, Long.class);
      final var result = query.getSingleResult();
      return Response.ok(result).build();
    } catch (final Exception ex) {
      return Response.status(Status.BAD_REQUEST).build();
    }
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  public Response createSample(final Sample sample) {
    final SampleEntity entity = new SampleEntity();
    fromSample(entity, sample);
    em.persist(entity);
    return Response.status(Status.CREATED).build();
  }

  @POST
  @Path("{sampleId}/measurement")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response createMeasurement( @PathParam("sampleId") final String sampleId, final Measurement measurement ) {
    final var sample = findBySampleId(sampleId);
    MeasurementEntity entity = sample.getMeasurementEntity();
    if (sample != null && entity == null)
    {
      entity = new MeasurementEntity();
      sample.setMeasurementEntity(entity);
      entity.setSampleEntity(sample);
    } 
    else throw new Error();
    fromMeasurement(entity, measurement); 
    final var eva = new Evaluation();
    final Result result = eva.evaluationMeasurement(entity.getTarget1(),entity.getTarget2(),entity.getTarget3(),entity.getReference());entity.setResult(result);
    if (entity.getId() == null) em.persist(entity);
    return Response.status(Status.CREATED).build();
  }

  private SampleEntity findBySampleId(final String sampleId) {
    final var jpaql = "SELECT s FROM Sample s WHERE s.sampleId = :sampleId";
    final var query = this.em.createQuery(jpaql, SampleEntity.class);
    query.setParameter("sampleId", sampleId);
    return query.getSingleResult();
  }

  private Sample allToSample(final SampleEntity sEntity) {
    final var sample = new Sample();
    sample.id = sEntity.getId();
    sample.sampleId = sEntity.getSampleId();
    sample.dateAndTime = sEntity.getDateAndTime();
    final var measurement = sEntity.getMeasurementEntity();
    if (measurement !=null)
    sample.result = measurement.getResult();

    return sample;
  }

  private void fromSample(final SampleEntity sEntity, final Sample sample) {
    sEntity.setSampleId(sample.sampleId);
    sEntity.setDateAndTime(formatDateTime());
    sEntity.setDescriptions(sample.descriptions);
  }

  private void fromMeasurement(final MeasurementEntity mEntity, final Measurement measurement) {
    mEntity.setTarget1(measurement.target1);
    mEntity.setTarget2(measurement.target2);
    mEntity.setTarget3(measurement.target3);
    mEntity.setReference(measurement.reference);
    mEntity.setResult(measurement.result);
  }

  private static String formatDateTime() {
    final var now = LocalDateTime.now(ZoneId.of("Europe/Berlin"));
    final var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    final var formatDateTime = now.format(formatter);

    return formatDateTime;
  }


}
