package com.qualitype.projectSamples;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class SampleApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {

        var registeredClasses = super.getClasses();

        var classes = new HashSet<Class<?>>();

        if (registeredClasses != null && !registeredClasses.isEmpty())
            classes.addAll(registeredClasses);

        classes.add(SamplesResource.class);

        return classes;
    }
}