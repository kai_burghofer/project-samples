package com.qualitype.projectSamples;

public class Evaluation {

  public Result evaluationMeasurement(final int target1, final int target2, final int target3, final int reference) {

    int numTarget = 0;
    
    if (setValue(target1)) numTarget++;
    if (setValue(target2)) numTarget++;
    if (setValue(target3)) numTarget++;

    final boolean ref = setValue(reference);

    if (ref) {
      if (numTarget >= 2) return Result.POSITIV;
      if (numTarget == 1) return Result.UNSICHER;
      return Result.NEGATIV;
    }
    return Result.UNGÜLTIG;
  }

  private static boolean setValue(final int n) {
    if (n <= 19) return false; 
    if (n <= 40) return true;
     return true;
  }
}
