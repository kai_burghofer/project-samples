package com.qualitype.projectSamples;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Table(name = "measurements", schema = "public")
@Entity

public class MeasurementEntity {

	private Integer id;
	private Integer target1;
	private Integer target2;
	private Integer target3;
	private Integer reference;
	private Result result;
	private SampleEntity sampleEntity;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "measurement_id_generator")
	@SequenceGenerator(name = "measurement_id_generator", sequenceName = "measurement_sequence", initialValue = 1, allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id){
		this.id=id;
	}

	@Column(name = "target1", updatable = false, nullable = false)
	public Integer getTarget1() {
		return this.target1;
	}

	public void setTarget1(Integer target1) {
		this.target1 = target1;
	}

	@Column(name = "target2", updatable = false, nullable = false)
	public Integer getTarget2() {
		return this.target2;
	}

	public void setTarget2(Integer target2) {
		this.target2 = target2;
	}

	@Column(name = "target3", updatable = false, nullable = false)
	public Integer getTarget3() {
		return this.target3;
	}

	public void setTarget3(Integer target3) {
		this.target3 = target3;
	}

	@Column(name = "reference", updatable = false, nullable = false)
	public Integer getReference() {
		return this.reference;
	}

	public void setReference(Integer reference) {
		this.reference = reference;
	}

	@Column(name = "result", updatable = false, nullable = false)
	@Enumerated(EnumType.STRING)
	public Result getResult() {
		return this.result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	@OneToOne
	@JoinColumn(name = "sample_id")
	public SampleEntity getSampleEntity() {
		return this.sampleEntity;
	}

	public void setSampleEntity(SampleEntity sampleEntity) {
		this.sampleEntity = sampleEntity;
	}
}