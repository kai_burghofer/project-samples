package com.qualitype.projectSamples;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Table(name = "samples", schema = "public")
@Entity(name = "Sample")
public class SampleEntity {

  private Integer id;
  private String sampleId;
  private String dateAndTime;
  private String descriptions;
  private MeasurementEntity measurementEntity;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sample_id_generator")
  @SequenceGenerator(name = "sample_id_generator", sequenceName = "sample_sequence", initialValue = 1, allocationSize = 1)
  @Column(name = "id", updatable = false, nullable = false, unique = true)
  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "sampleId", updatable = false, nullable = false)
  public String getSampleId() {
    return this.sampleId;
  }

  public void setSampleId(String sampleId) {
    this.sampleId = sampleId;
  }

  @Column(name = "dateAndTime", updatable = false, nullable = false)
  public String getDateAndTime() {
    return this.dateAndTime;
  }

  public void setDateAndTime(String dateAndTime) {
    this.dateAndTime = dateAndTime;
  }

  @Column(name = "descriptions", updatable = false, nullable = false)
  public String getDescriptions() {
    return this.descriptions;
  }

  public void setDescriptions(String descriptions) {
    this.descriptions = descriptions;
  }

  @OneToOne(mappedBy = "sampleEntity")
  public MeasurementEntity getMeasurementEntity() {
    return this.measurementEntity;
  }

  public void setMeasurementEntity(MeasurementEntity measurementEntity) {
    this.measurementEntity = measurementEntity;
  }
}
