package com.qualitype.projectSamples;

public enum Result {
    NEGATIV, POSITIV, UNSICHER, UNGÜLTIG
}