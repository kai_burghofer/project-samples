CREATE TABLE samples
(
    id INTEGER PRIMARY KEY NOT NULL,
    sampleId VARCHAR(20) NOT NULL UNIQUE,
    dateAndTime VARCHAR NOT NULL,
    descriptions VARCHAR(200)
    
);

CREATE TABLE measurements
(
    id INTEGER PRIMARY KEY NOT NULL,
    target1 INTEGER NOT NULL,
    target2 INTEGER NOT NULL,
    target3 INTEGER NOT NULL,
    reference INTEGER NOT NULL,
    result VARCHAR,
    sample_id INTEGER,
    FOREIGN KEY(sample_id)
    REFERENCES samples(id) 
  
);


CREATE SEQUENCE sample_sequence INCREMENT 1 START 1;

CREATE SEQUENCE measurement_sequence INCREMENT 1 START 1;

INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-492-DD4', '2020-11-23 11:35', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-529-DD5', '2020-11-23 10:45', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-027-DD5', '2020-11-25 08:22', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-549-DD7', '2020-11-25 14:15', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-192-DD2', '2020-12-26 17:47', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-035-DD5', '2020-12-27 14:45', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-924-DD6', '2020-12-28 10:11', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-658-DD4', '2020-12-29 16:45', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-067-DD4', '2020-12-30 09:17', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-748-DD4', '2021-01-01 10:18', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-455-DD8', '2021-01-02 15:24', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-766-DD8', '2021-01-02 16:28', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-921-DD2', '2021-01-02 10:58', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-998-DD7', '2021-01-04 07:45', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-214-DD7', '2021-01-06 12:57', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-544-DD3', '2021-01-06 13:49', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-992-DD3', '2021-02-01 14:15', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-277-DD1', '2021-02-01 14:44', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-742-DD2', '2021-02-02 11:55', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-639-DD2', '2021-02-02 09:25', null);
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-312-DD9', '2021-02-03 10:55', 'Ergebnis noch heute an Dr. Frank übermitteln');
INSERT INTO public.samples(id, sampleId, dateAndTime, descriptions) VALUES (nextval('sample_sequence'),'C19-366-DD9', '2021-02-03 08:09', 'Ergebnis an Dr. Schmidt übermitteln');


INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'3', '2', '5', '35', 'NEGATIV','4');
INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'33', '22', '5', '35', 'POSITIV','17');
INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'33', '22', '5', '35', 'POSITIV','20');
INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'33', '22', '5', '35', 'POSITIV','13');
INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'33', '2', '5', '35', 'UNSICHER','6');
INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'33', '22', '5', '5', 'UNGÜLTIG','5');
INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'3', '2', '5', '35', 'NEGATIV','7');
INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'3', '2', '5', '35', 'NEGATIV','8');
INSERT INTO public.measurements(id, target1, target2, target3, reference, result, sample_id) VALUES (nextval('measurement_sequence'),'3', '22', '5', '35', 'UNSICHER','11');